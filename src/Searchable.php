<?php

namespace Jackwebs\Eloquentx;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

trait Searchable
{

    public $pagination;

    //public static $searchColumns;
    //public static $searchRelations;
    public function getSearchColumns()
    {
        return isset($this->searchColumns) ? $this->searchColumns : Schema::getColumnListing($this->table);
    }

    public function getSearchRelations()
    {
        return isset($this->searchRelations) ? $this->searchRelations : [];
    }

    public function scopeSearch($query, $searchtext, $limit = 0)
    {
        $searchterms = explode(" ", $searchtext);

        foreach ($searchterms as $payload) {
            $query->where(function ($query) use ($payload) {


                $query->where($this->primaryKey, 'LIKE', '%' . $payload . '%');

                $columns = $this->getSearchColumns();

                foreach ($columns as $attribute) {
                    $query->orWhere($attribute, 'LIKE', '%' . $payload . '%');
                };

                foreach ($this->getSearchRelations() as $r_name => $r_cols) {
                    $query->orWhereHas($r_name, function ($query) use ($payload, $r_cols) {
                        $first = true;
                        foreach ($r_cols as $col) {
                            if ($first) {
                                $query->where($col, 'LIKE', '%' . $payload . '%');
                            } else {
                                $query->orWhere($col, 'LIKE', '%' . $payload . '%');
                            }

                            $first = false;
                        }
                        //$query->orWhere('lastname', 'LIKE', '%' . $payload . '%');
                    });
                }
            });
        }
        if ($limit > 0) {
            return $query->take($limit);
        } else {
            return $query->paginate($this->pagination)->appends([
                'search' => $payload
            ]);
        }
    }

    public function scopeFilter($query, $criteria, $limit = 0)
    {
        $cols = $this->getSearchColumns();

        foreach ($criteria as $field => $value) {
            if (empty($value)) {
                continue;
            }
            if (in_array($field, $cols)) {
                if (is_array($value)) {
                    $query->whereIn($field, $value);
                } else {
                    $query->where($field, $value);
                }
            } else {
                $ar = explode('_', $criteria);
                $op = array_pop($ar);

                if (in_array($op, ['eq', 'gt', 'gte', 'lt', 'lte'])) {
                    $col = join('_', $ar);

                    $op = str_replace(
                        ['eq', 'gte', 'gt', 'lte', 'lt'],
                        ['=', '>=', '>', '<=', '<'],
                        $op
                    );

                    if (in_array($col, $cols)) {
                        $query->where($col, $op, $value);
                    }
                }
            }
        }
        if ($limit > 0) {
            return $query->take($limit);
        } else {
            return $query->paginate($this->pagination)->appends($criteria);
        }
    }
}
