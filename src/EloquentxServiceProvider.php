<?php

namespace Jackwebs\Eloquentx;

use Illuminate\Support\ServiceProvider;

class EloquentxServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        //$this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }
    /**
     * Register the application services.
     */
    public function register()
    {
        $this->mergeConfigFrom(
            __DIR__ . '/../config/eloquentx.php',
            'eloquentx'
        );
    }
}
